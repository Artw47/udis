#%%
#ИМПОРТ БИБЛИОТЕК
import matplotlib.pyplot as plt
import numpy as np
from config_checker import *
from scipy.integrate import solve_ivp

#%% 
#ИНИЦИАЛИЗАЦИЯ ФУНКЦИЙ

#Управляющий сигнал
def sigma(x: float, y: float) -> float:
    """Управляющая функция
    ---------
    Args:
        x (float): координата угла
        y (float): угловая скорость

    Returns:
        float: управляющая функция
    """
    return x+k*y

#Управляющая функция

#Буферная переменная - для моделирования гистерезиса.
#В ней хранится предыдущее значение F(sigma)
gist_buff=0

def F(sigma: float) -> float:
    """Управляющая функция
    
    Args:
        sigma (float): управляющий сигнал

    Returns:
        float: значение управляющей функции
    """
    global gist_buff

    if abs(sigma)>=alpha:
        result=-np.sign(sigma)

    elif abs(sigma)<alpha-h:
        result=0

    else:
        result=gist_buff
    
    gist_buff=result
    return result

#нулевые функции
def x_zero(t, y) -> float:
    return y[0]
x_zero.terminal = True
x_zero.direction = -1

def y_zero(t, y) -> float:
    return y[1]
y_zero.terminal = True
y_zero.direction = -1

def F_zero(t, y) -> float:
    return F(sigma(y[0], y[1]))
F_zero.terminal = True
F_zero.direction = -1

#%%
#ДУ динамики КА

#при управлении двигателем:
def dy_dt_e(t, y) -> np.ndarray:
    result=np.array([y[1], a_upr*F(sigma(y[0], y[1]))+g_vozm(y), FUEL_CONSUMPTION*abs(F(sigma(y[0], y[1])))])
    return result

#при управлении маховиком:
def dy_dt_m(t, y) -> np.ndarray:
    result=np.array([y[1], a_upr*F(sigma(y[0], y[1]))+g_vozm(y), -a_upr*F(sigma(y[0], y[1]))])
    return result

def graf_sud(y0: list, t_span: list, _alpha, _h, _k, _a, dt=DT, stop_events=None, manager="engine", _g=lambda y:0, osc=False):
    """Построение графиков
    -------
    Args:
        y0 (list): начальные условия [x0, y0]
        t_span (list): Диапазон [t0, tk]
        _alpha (_type_): уставка
        _h (_type_): ширина петли гистерезиса
        _k (_type_): коэф. опережения по фазе (коэф. демпфирования)
        _a_upr (_type_): эфф. управления (в рад/с)
        dt (_type_, optional): Шаг интегрирования. По умолчанию DT.
        stop_events (_type_, optional): События, который останавливают интегрирование. По умолчанию None.
        manager (str, optional): Управляем ДМТ или маховиком. По умолчанию "engine".
    """
    gist_buff=0
    if manager=="engine":
        osc_nums=[0, 1, 2, 3, 5]
        dy_dt=dy_dt_e

    elif manager=="mahovik":
        osc_nums=[0, 1, 2, 3, 4]
        dy_dt=dy_dt_m

    # Для того чтобы не изменялся исходный список y0, создадим его копию для использования внутри функции
    y0=[*y0]
    y0.append(0)

    #установка параметров СУД
    global alpha, h, k, a_upr, g_vozm
    alpha=_alpha     #уставка
    h=_h         #ширина петли гистерезиса
    k=_k
    a_upr=_a*180/np.pi
    g_vozm = lambda y: _g(y)*180/np.pi

    #решение ДУ
    sol = solve_ivp(fun=dy_dt, t_span=t_span, y0=y0, max_step=dt, events=stop_events)
    #распаковка решения по массивам
    x, y, a = sol.y
    x, y = np.array([x, y])
    t=sol.t

    #ПОСТРОЕНИЕ ГРАФИКОВ
    #массив данных для построения графиков
    GRAFS_LIST=[
        [t, x, 'Осциллограмма угловой координаты', 't, c', r'$x, град$'],
        [t, y, 'Осциллограмма угловой скорости', 't, c', r'$y, град/с$'],
        [t, [sigma(i, j) for i, j in zip(x, y)], 'Осциллограмма управляющего сигнала', 't, c', ''],
        [t, [F(i) for i in [sigma(i, j) for i, j in zip(x, y)]], 'Осциллограмма управляющей функции', 't, c', ''],
        [t, a, 'Осциллограмма момента импульса маховика', 't, c', r'$кг{*}м^2{*}с^{-1}$'],
        [t, a, 'Осциллограмма расхода топлива', 't, c', 'г'],
    ]

    grafs_list=[GRAFS_LIST[i] for i in osc_nums]

    #построение фазового портрета
    phase, axes = plt.subplots()
    axes.set_title('Фазовый портрет', {'fontsize': 10})
    axes.set_xlabel(r'$x, град$', {'fontsize': 8})
    axes.set_ylabel(r'$y, град/с$', {'fontsize': 8})
    axes.grid()

    min_x=1.1*min(-alpha, np.min(x))
    max_x=1.1*max(alpha, np.max(x))
    min_y=1.1*np.min(y)
    max_y=1.1*np.max(y)

    axes.set_xlim(min_x, max_x)
    axes.set_ylim(min_y, max_y)

    axes.plot(x, y)

    line_on=np.array([[-k*i+alpha, i] for i in np.arange(-200,200,1)])

    line_off=line_on.copy()
    line_off[:,0]-=h


    axes.plot(line_off[:,0], line_on[:,1], color='g', lw=0.5)
    axes.plot(-line_off[:,0], -line_on[:,1], color='g', lw=0.5)
    axes.plot(line_on[:,0], line_off[:,1], color='orange', lw=0.5)
    axes.plot(-line_on[:,0], -line_off[:,1], color='orange', lw=0.5)

    #ПОСТРОЕНИЕ ОСЦИЛЛОГРАММ
    def graf_osc():
        osc, axes = plt.subplots(len(osc_nums), figsize=(6, 9), tight_layout=True)
        
        i=0
        for t, values, name, xlabel, ylabel in grafs_list:
            axes[i].set_title(name, {'fontsize': 10})
            axes[i].set_xlabel(xlabel, {'fontsize': 8})
            axes[i].set_ylabel(ylabel, {'fontsize': 8})
            axes[i].grid()
            axes[i].plot(t, values)
            i+=1
    
    if osc: graf_osc()
    print(f'tk={t[-1]}\tрасход топлива={a[-1]}')
#%%
