import numpy as np
from config_checker import *
from matplotlib import pyplot as plt
from sat_functions import *

ch_names=dict([(1,'Крен'), (2,'Курс'), (3,'Тангаж')])

func_names={
    m_grav_B: 'Гравитационный момент',
    m_gyro_B: 'Гироскопический момент',
    m_aero_B: 'Аэродинамический момент',
    m_light_B: 'Момент светового давления',
    m_magn_B: 'Момент магнитного взаимодействия',
    m_summ_B: 'Суммарный момент',
}


def m_full_B(f, axis, start_stop_step: list = [-180, 180, 0.5], channels="123", title='name_of_func'):
    """Построение графика момента в зависимости от угла поворота.

    Args:
    ----
        f (_type_): Функция момента.
        axis (_type_): Направление оси, в зависимости
        от угла по которой выводится график
        

        channels (str, optional):Каналы, для которых требуется построить график.
        Указываются цифрами: 1 - крен, 2 - курс, 3 - тангаж.
        По умолчанию график строится для всех каналов.
        Графики строятся в указанном порядке, т.е. значение "123" означает,
        что первый график - крен, второй - курс, третий - тангаж.
        

        start_stop_step (list, optional): Для какого интервала угла строить график.
        По умолчанию [-180, 180, 0.5].


        title (str, optional): Заголовок рисунка. По умолчанию заголовка нет.
    """
    sol=[[i, *f(Quaternion(degrees=i, axis=axis))] for i in np.arange(*start_stop_step)]

    sol=np.array(sol)
    plt.figure(figsize=(8, 5))
    
    if title=='name_of_func':
        title=func_names[f]
    plt.title(title)

    for channel in channels:
        i=int(channel)
        plt.plot(sol[:,0], sol[:,i], label=ch_names[i])

    plt.grid()
    plt.xlabel("град")
    plt.ylabel("М, Н*м")
    plt.legend()
    plt.show()

def all_single_axis(axis: list):
    """Вывод графиков всех моментов"""
    for func in func_names:
        m_full_B(func, axis, title='name_of_func')

def B_B():
    """Строит график вектора магнитной индукции в зависимости от магнитной широты.
    """
    sol=[[i, *f_B(Quaternion(degrees=0, axis=[0, 0, 1]), F=i/180*np.pi)] for i in np.arange(*[-90, 90, 0.5])]

    sol=np.array(sol)
    plt.figure(figsize=(8, 5))
    plt.plot(sol[:,0], sol[:,1], label='x')
    plt.plot(sol[:,0], sol[:,2], label='y')
    plt.plot(sol[:,0], sol[:,3], label='z')
    plt.grid()
    plt.legend()
    plt.title('Проекции вектора магнитной индукции')
    plt.xlabel("Ф, град")
    plt.ylabel("B, Тл")
    plt.show()