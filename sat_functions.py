import numpy as np
from pyquaternion import Quaternion

from config_checker import *

#Функция матрицы поворота
def a_matrix(q: Quaternion) -> np.ndarray:
    """Матрица поворота
    --------

    Args:
        q (Quaternion): кватернион поворота

    Returns:
        np.ndarray: матрица поворота
    """
    A=q.rotation_matrix
    return A

#Функция кватерниона поворота для быстрого ввода
def quat(deg:float, axis:list) -> Quaternion:
    """Быстрое получение кватерниона

    Args:
        deg (float): угол в градусах
        axis (list): ось

    Returns:
        Quaternion: кватернион
    """
    return Quaternion(degrees=deg, axis=axis)

#Кинетический момент корпуса КА
def K_B(omega: np.ndarray) -> np.ndarray:
    """Момент импульса корпуса КА
    -----------

    Args:
        omega (np.ndarray): вектор угловых скоростей

    Returns:
        np.ndarray: момент импульса корпуса КА
    """
    out=J_B.dot(omega)
    return out

#Гироскопический момент корпуса КА
def m_gyro_B(q: Quaternion) -> np.ndarray:
    """Гироскопический момент
    ------
    Args:
        omega (np.ndarray): вектор угловой скорости
        K (np.ndarray): момент импульса корпуса КА

    Returns:
        np.ndarray: вектор гироскопического момента
    """
    ebn=q.inverse.rotate(np.array([0, 0, 1]))
    m_gyr=-OMEGA_ORB**2*np.cross(ebn, J_B.dot(ebn))
    return m_gyr

#Гироскопический момент корпуса КА (в зависимости от скорости а не координат)
# def m_gyro_B(omega: np.ndarray, K: np.ndarray) -> np.ndarray:
#     """Гироскопический момент
#     ------
#     Args:
#         omega (np.ndarray): вектор угловой скорости
#         K (np.ndarray): момент импульса корпуса КА

#     Returns:
#         np.ndarray: вектор гироскопического момента
#     """
#     out=np.cross(omega, K)
#     return out

#гравитационный момент
def m_grav_B(q: Quaternion) -> np.ndarray:
    """Гравитационный момент
    --------
    Args:
        q (Quaternion): кватернион состояния

    Returns:
        np.ndarray: гравитационный момент
    """
    er=q.inverse.rotate(np.array([0, 1, 0]))
    m_g=3*OMEGA_ORB**2*np.cross(er, J_B.dot(er))
    return m_g

#индукция магнитного поля
def f_B(q: Quaternion, F=FI) -> np.ndarray:
    """Вектор индукции магнитного поля
    -----

    Args:
        q (Quaternion): кватернион состояния
        F (_type_, optional): магнитная широта. По умолчанию задана в константах

    Returns:
        np.ndarray: вектор индукции магнитного поля
    """
    Theta=np.pi/2-F
    u0=1.257*10**(-6)
  
    et=q.inverse.rotate(np.array([0, 1, 0]))
    er=q.inverse.rotate(np.array([0, 0, 1]))

    B=u0*MAGN_DP/(4*np.pi*R_ORB**3)*(2*np.cos(Theta)*er+np.sin(Theta)*et)
    return B

#магнитный момент
def m_magn_B(q: Quaternion, FI=FI) -> np.ndarray:
    """Магнитный момент
    ----------

    Args:
        q (Quaternion): кватернион состояния
        F (_type_, optional): магнитная широта. По умолчанию задана в константах.

    Returns:
        np.ndarray: вектор магнитного момента
    """
    B=f_B(q, FI)
    m_magn=np.cross(P_B, B)
    return m_magn

#момент от солнечного давления
def m_light_B(q: Quaternion, Surface_list=SURFACES) ->np.ndarray:
    """Момент светового давления
    ------------

    Args:
        q (Quaternion): кватернион состояния
        Surface_list (_type_, optional): Список поверхностей КА. По умолчанию Surfaces.

    Returns:
        np.ndarray: вектор момента светового давления
    """
    v_light=np.array([0, -1, 0])
    v_light=v_light/np.linalg.norm(v_light)
    v_light=q.inverse.rotate(v_light)      #вектор солнечного потока

    R_earth=149.59787*10**9 #радиус орбиты Земли
    E0=1367     #солнечная постоянная для орбиты Земли
    c=300000000 #скорость света

    epsilon=0.5       #коэффициент черноты поверхностей (берём средний)

    pressure=E0/c*(R_P_ORB/R_earth)**2   #солнечное давление на единицу площади

    m_light=np.array([0, 0, 0])

    for surface in Surface_list:
        s=surface[0]
        r=np.array(surface[1:4])
        n=np.array(surface[4:])

        F=s*pressure*v_light*abs(v_light.dot(n))*(1+epsilon)    #сила давления на поверхность
        L=r-R_CM    #центр масс КА в строительной СК
        M=-np.cross(L, F)   #момент, создаваемый от поверхности

        m_light=m_light+M   #добавляем в суммарный момент

    return m_light

#момент от аэродинамического давления
def m_aero_B(q: Quaternion, Surface_list=SURFACES) ->np.ndarray:
    """Аэродинамический момент
    -------------

    Args:
        q (Quaternion): кватернион состояния
        Surface_list (_type_, optional): Список поверхностей КА. По умолчанию Surfaces.

    Returns:
        np.ndarray: вектор аэродинамического момента
    """
    v_aero=np.array([-1, 0, 0])
    v_aero=v_aero/np.linalg.norm(v_aero)
    v_aero=q.inverse.rotate(v_aero)      #вектор солнечного потока

    pressure=1/2*RO*(OMEGA_ORB*R_ORB)**2   #давление потока на единицу площади

    m_aero=np.array([0, 0, 0])

    for surface in Surface_list:
        s=surface[0]
        r=np.array(surface[1:4])
        n=np.array(surface[4:])

        F=s*pressure*v_aero*abs(v_aero.dot(n))    #сила давления на поверхность
        L=r-R_CM            #центр масс КА в строительной СК
        M=-np.cross(L, F)   #момент, создаваемый от поверхности

        m_aero=m_aero+M   #добавляем в суммарный момент

    return m_aero

def m_summ_B(q: Quaternion) -> np.ndarray:
    """Суммарный момент
    ------

    Args:
        q (Quaternion): кватернион состояния

    Returns:
        np.ndarray: вектор суммарного момента
    """
    m_summ_B=m_grav_B(q)+m_magn_B(q)+m_aero_B(q)+m_light_B(q)
    return m_summ_B