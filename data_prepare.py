import numpy as np


#Тензор инерции функция
def f_J(J_input: str) -> np.ndarray:
    """Тензор инерции
    ---------------
    Args:
        J_input (str): строка

    Returns:
        np.ndarray: тензор инерции
    """
    J_input=J_input.replace(',', '.')

    stay_list=['.', '\t', ' ', '-']

    for i in J_input:
        delete=not (i.isdigit() or (i in stay_list))
        if delete:
            J_input=J_input.replace(i, ' ')

    J_out=J_input.replace(' ', '\t').split('\t')

    while '' in J_out:
        J_out.remove('')
    J_out=-np.array([*map(float, J_out)]).reshape(3, 3)

    for i in range(3):
        J_out[i, i]=-J_out[i, i]

    return J_out